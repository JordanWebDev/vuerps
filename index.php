<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/index.css" />
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <title>Rock Paper Scissors</title>
    </head>
    <body>
        <main>
            <section>
                <div class="container">
                    <div class="jumbotron">

                        <h1 id="main-title">Rock Paper Scissors</h1>

                        <div id="player-selection">
                            <label>Player 1, Enter Name:</label>
                            <input v-model="playerOne" @keyup.enter="enterName"/>
                            <label>Player 2, Enter Name:</label>
                            <input v-model="playerTwo" @keyup.enter="enterName"/>
                            <button @click="enterName">Submit</button>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="sel1">Select One:</label>
                                    <select class="form-control" id="sel1">
                                        <option>Rock</option>
                                        <option>Paper</option>
                                        <option>Scissors</option>
                                    </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                    <label for="sel1">Select One:</label>
                                    <select class="form-control" id="sel1">
                                        <option>Rock</option>
                                        <option>Paper</option>
                                        <option>Scissors</option>
                                    </select>
                                    </div>
                                </div>
                                <button @click="battle">Battle!</button>
                            </div>
                            <div class="who-won">
                                <h2> {{ playerOne }} vs. {{ playerTwo }}</h2>

                            </div>
                        </div>

                    </div>
                </div>

            </section>
        </main>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <script type="text/javascript" src="javascript/vue.js"></script>
        <script type="text/javascript" src="javascript/index.js"></script>
    </body>
</html>
